var amqp = require('amqplib/callback_api');
const nodemailer = require("nodemailer");

async function sendTranoodMail(dataString) {
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "tranood2@gmail.com",
      pass: "meseriash123$"
    }
  });

  const data = JSON.parse(dataString);

  const bodyText = `You have a new match on Tranood! Refugee - name: ${data.refugee.name}, email: ${data.refugee.email}. Donator - name: ${data.donator.name}, email: ${data.donator.email}. ` + "Item Name: " + data.item.name + ", Item Tag: " + data.item.tag + ", Item Description: " + data.item.description + ", Comment: " + data.item.accepted.comment;

  var mailOptions = {
    from: "tranood2@gmail.com",
    to: `${data.refugee.email}, ${data.donator.email}`,
    subject: 'New message on Tranood',
    text: bodyText,
    html: `<p>${bodyText}</p>`
  };
  
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
  });
}

amqp.connect('amqp://34.118.53.98', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      console.log(error1);
    }
    var queue = 'hello';

    channel.assertQueue(queue, {
      durable: true
    });

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

    channel.consume(queue, async function(msg) {
        console.log(" [x] Received %s", msg.content.toString());
        
        await sendTranoodMail(msg.content.toString());
    }, {
        noAck: true
    });
  });
});