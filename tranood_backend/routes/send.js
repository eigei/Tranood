var amqp = require('amqplib/callback_api');

amqp.connect('amqp://34.118.53.98', function(error0, connection) {
  if (error0) {
    console.log("erroare");
    throw error0;
  } else if (connection) {
    console.log("Connection successful");
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var queue = 'hello';
    var msg = 'Hello from Tranood';

    channel.assertQueue(queue, {
      durable: true
    });

    channel.sendToQueue(queue, Buffer.from(msg));
    console.log(" [x] Sent %s", msg);
  });
});
