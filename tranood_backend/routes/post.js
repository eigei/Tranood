const router = require("express").Router()
const User = require("../models/User")
const Post = require("../models/Post")
const Item = require("../models/Item")
const { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin, authTokenVerifyMiddleware } = require("./verifyToken")

// Create a Post
router.post("/", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const post = new Post({author: req.user.uid})
    post.save(function (err) {
      if (err) return res.status(500).json(err);
    });
    const items = req.body.items;
    items.forEach(item => {
      const newItem = new Item({postId: post._id, name: item.name, tag: item.tag, description: item.description})
      newItem.save(function (err) {
        if (err) return res.status(500).json(err);
      });
    })
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Get all Posts
router.get("/", async (req, res) => {
  try {
    const post = await Post.find({});
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
});
// Get all Posts of an User
router.get("/user/:user_id", async (req, res) => {
  try {
    const post = await Post.find({author: req.params.user_id});
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Get all Posts with all their Items
router.get("/items", async (req, res) => {
  const active = (req.query.active === 'true');
  try {
    const posts = await Post.find({});
    let arr = await Promise.all(posts.map(async post => {
      let el = {};
      el["post"] = post;
      const items = await Item.find({postId: post._id});
      if (active) {
        let activeCount = 0;
        for (const index in items) {
          if (items[index].active) {
            activeCount = activeCount+1;
          }
        }
        el["items"] = items;
        if (activeCount>0) {
          return el;
        } else {
          return null;
        }
      } else {
        el["items"] = items;
        return el;
      }
    }));
    if (active) {
      arr = arr.filter(function(post) {
        if (post == null) {
          return null;
        } else {
          return true;
        }
      })
    }
    res.status(200).json(arr);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Get all Posts of an User, with their Items
router.get("/items/user/:user_id", async (req, res) => {
  const active = (req.query.active === 'true');
  try {
    const posts = await Post.find({author: req.params.user_id});
    let arr = await Promise.all(posts.map(async post => {
      let el = {};
      el["post"] = post;
      const items = await Item.find({postId: post._id});
      if (active) {
        let activeCount = 0;
        for (const index in items) {
          if (items[index].active) {
            activeCount = activeCount+1;
          }
        }
        el["items"] = items;
        if (activeCount>0) {
          return el;
        } else {
          return null;
        }
      } else {
        el["items"] = items;
        return el;
      }
    }));
    if (active) {
      arr = arr.filter(function(post) {
        if (post == null) {
          return null;
        } else {
          return true;
        }
      })
    }
    res.status(200).json(arr);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Get all Items of a Post
router.get("/:id/items", async (req, res) => {
  try {
    const items = await Item.find({postId: req.params.id});
    res.status(200).json(items);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Get a Post
router.get("/:id", async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Delete a Post
router.delete("/:id", authTokenVerifyMiddleware, async (req, res) => {
  try {
    await Item.deleteMany({postId: req.params.id});
    await Post.findByIdAndDelete(req.params.id);
    res.status(200).json("Post deleted.");
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router