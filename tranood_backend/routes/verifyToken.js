const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  const authHeader = req.headers.token;
  if (authHeader) {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, process.env.JWT_SEC, (err, user) => {
      if (err) res.status(403).json("Token is not valid!");
      req.user = user;
      next();
    });
  } else {
    return res.status(401).json("You are not authenticated!");
  }
};

const verifyTokenAndAuthorization = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.id === req.params.id || req.user.isAdmin) {
      next();
    } else {
      res.status(403).json("You are not alowed to do that!");
    }
  });
};

const verifyTokenAndAdmin = (req, res, next) => {
  verifyToken(req, res, () => {
    if (req.user.isAdmin) {
      next();
    } else {
      res.status(403).json("You are not alowed to do that!");
    }
  });
};

let firebaseApp = null;

const authTokenVerifyMiddleware = (req, res, next) => {
  const admin = require("firebase-admin");
  let serviceAccount = require("../keys/tranood-development-firebase-adminsdk-s1onl-64c1f6e21e.json");
  if (!firebaseApp) {
    firebaseApp = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount)
    });
  }
  const tokenString = req.headers['authorization'] ? req.headers['authorization'].split(" ") : null
  if (!tokenString) {
    res.send("No header provided");
  } else if(!tokenString[1]) {
    res.send("No token provided");
  } else {
    const {getAuth} = require("firebase-admin/auth")
    getAuth().verifyIdToken(tokenString[1])
    .then((decodedToken) => {
      req.user = decodedToken;
      next();
    })
    .catch((error) => {
      res.send(error);
    });
  }
}

module.exports = {
  verifyToken,
  verifyTokenAndAuthorization,
  verifyTokenAndAdmin,
  authTokenVerifyMiddleware
};