const router = require("express").Router()
const User = require("../models/User")
const Post = require("../models/Post")
const Item = require("../models/Item")
var amqp = require('amqplib/callback_api')
const { authTokenVerifyMiddleware } = require("./verifyToken")

// Get an Item
router.get("/:id", async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    res.status(200).json(item);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Create an Item
router.post("/", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const item = new Item({postId: req.body.postId, name: req.body.name, tag: req.body.tag, description: req.body.description})
    item.save(function (err) {
      if (err) return res.status(500).json(err);
    });
    res.status(200).json(item);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Request an Item
router.put("/:id/request", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    const user = req.user.uid;
    if (!item.requested.includes(user)) {
      oldRequested = [...item.requested];
      oldRequested.push(user);
      item.set("requested", oldRequested);
      item.save();
    }
    res.status(200).json(item);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Accept a User that has requested an Item
router.put("/:id/accept/:user_id", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    const post = await Post.findById(item.postId);
    const userAccepting = req.user.uid;
    const userAccepted = await User.findOne({firebase_uid: req.params.user_id}).exec(); // donatorul
    console.log(userAccepted.name, userAccepted.mail);
    const userAcceptingObject = await User.findOne({firebase_uid: userAccepting}).exec();  /// refugiatul
    if (item.requested.includes(req.params.user_id) && post.author == userAccepting && item.active) {
      const acc = {};
      acc["user"] = userAccepted;
      acc["comment"] = req.body.comment;
      item.set("accepted", acc);
      item.set("active", false);
      item.save();
      const mailData = {};
      const mailDataRefugee = {};
      mailDataRefugee["name"] = userAcceptingObject.name;
      mailDataRefugee["email"] = userAcceptingObject.email;
      const mailDataDonator = {};
      mailDataDonator["name"] = userAccepted.name;
      mailDataDonator["email"] = userAccepted.email;
      mailData["refugee"] = mailDataRefugee;
      mailData["donator"] = mailDataDonator;
      mailData["item"] = item;
      const mailDataString = JSON.stringify(mailData);

      // Send message to mailing server
      amqp.connect('amqp://34.118.53.98', function(error0, connection) {
        if (error0) {
          console.log("erroare");
          throw error0;
        } else if (connection) {
          console.log("Connection successful");
        }
        connection.createChannel(function(error1, channel) {
          if (error1) {
            throw error1;
          }
          var queue = 'hello';

          channel.assertQueue(queue, {
            durable: true
          });

          channel.sendToQueue(queue, Buffer.from(mailDataString));
          console.log(" [x] Sent %s", mailDataString);
        });
      });
    }
    res.status(200).json(item);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Update an Item
router.patch("/:id", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);
    if (req.body.name) {
      item.set("name", req.body.name)
    }
    if (req.body.tag) {
      item.set("tag", req.body.tag)
    }
    if (req.body.description) {
      item.set("description", req.body.description)
    }
    if (req.body.active != null) {
      item.set("active", req.body.active)
    }
    item.save()
    res.status(200).json(item);
  } catch (err) {
    res.status(500).json(err);
  }
});

// Delete an Item
router.delete("/:id", authTokenVerifyMiddleware, async (req, res) => {
  try {
    await Item.findByIdAndDelete(req.params.id);
    res.status(200).json("Item deleted.");
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router