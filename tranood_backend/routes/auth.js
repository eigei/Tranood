const router = require("express").Router()
const User = require("../models/User")
const CryptoJS = require("crypto-js")
const jwt = require("jsonwebtoken");
const {authTokenVerifyMiddleware} = require("./verifyToken")

router.get("/token", authTokenVerifyMiddleware, async (req, res) => {
  res.send(req.user.uid);
});

//REGISTER
router.post("/register", async (req, res) => {
  const newUser = new User({
    name: req.body.name,
    email: req.body.email,
    role: req.body.role,
    firebase_uid: req.body.firebase_uid,
  });

  try {
    const savedUser = await newUser.save();
    res.status(201).json(savedUser);
  } catch (err) {
    res.status(500).json(err);
  }
})

// GET Profile
router.get("/profile", authTokenVerifyMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ firebase_uid: req.user.uid })
    res.status(200).json(user)
  } catch (err) {
    res.status(500).json(err)
  }
})

module.exports = router