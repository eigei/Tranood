const mongoose = require("mongoose")

const ItemSchema = new mongoose.Schema(
  {
    postId: { type: String, required: true },
    name: { type: String, required: true },
    tag: { type: String, enum: ['shelter', 'transport', 'donation'], required: true },
    description: { type: String },
    requested: { type: Array },
    accepted: { type: Object },
    active: {type: Boolean, default: true },
  },
  { timestamps: true }
)

module.exports = mongoose.model("Item", ItemSchema)
