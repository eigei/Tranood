const mongoose = require("mongoose")

const UserSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    role: { type: String, enum: ['refugee', 'donator', 'admin'], required: true },
    firebase_uid: { type: String, required: true, unique: true },
    image: { type: String },
  },
  { timestamps: true }
)

module.exports = mongoose.model("User", UserSchema)
