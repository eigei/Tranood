const express = require("express")
const app = express()
const mongoose = require("mongoose")
const dotenv = require("dotenv")
const userRoute = require("./routes/user")
const postRoute = require("./routes/post")
const itemRoute = require("./routes/item")
const authRoute = require("./routes/auth")
const cors = require("cors")

dotenv.config()

mongoose
  .connect(process.env.MONGO_URL)
  .then(() => console.log("DB Connection Successfull!"))
  .catch((err) => {
    console.log(err)
  })

app.use(cors())
app.use(express.json())
app.use(express.static("public"))
app.use("/api/auth", authRoute)
app.use("/api/users", userRoute)
app.use("/api/posts", postRoute)
app.use("/api/items", itemRoute)

app.listen(process.env.PORT || 5000, () => {
  console.log("Backend server is running!")
})