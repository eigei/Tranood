import styled from "styled-components"
import { Link } from "react-router-dom"

const Container = styled(Link)`
  height: 30px;
  background: linear-gradient(
    0deg,
    #fed400ba,
    #015bbbb4
  );
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 3px;
  &:focus, &:visited, &:hover, &:link, &:active {
  text-decoration: none;
  }
`

const Announcement = () => {
  return (
    <Container component={Link} to={'/support_ukraine'}>WE STAND BY UKRAINE</Container>
  )
}

export default Announcement