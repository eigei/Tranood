import { ArrowLeftOutlined, ArrowRightOutlined } from "@material-ui/icons"
import styled from "styled-components"
import { Link } from "react-router-dom"
import { useState } from "react";
import { sliderItems } from "../data"
import {mobile} from "../responsive"
import { primary } from '../constants/colors';
import { useAuth } from "../contexts/AuthContext";

const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  overflow: hidden;
  position: relative;
  ${mobile({ display: "none" })}
`
const Arrow = styled.div`
  width: 50px;
  height: 50px;
  background-color: aliceblue;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${props=> props.direction === "left" && "10px"};
  right: ${props=> props.direction === "right" && "10px"};
  margin: auto;
  cursor: pointer;
  opacity: 0.5;
  z-index: 2;
`
const Wrapper = styled.div`
  height: 100%;
  display: flex;
  transition: all 1.5s ease;
  transform: translateX(${(props) => props.slideIndex * -100}vw);
`
const Slide = styled.div`
  width: 100vw;
  height: 92vh;
  display: flex;
  align-items: center;
  background-color: #${(props) => props.bg};
`
const ImgContainer = styled.div`
  height: 100%;
  flex: 1;
`
const Image = styled.img`
  height: 100%;
`
const InfoContainer = styled.div`
  flex: 1;
  padding: 50px;
`
const Title = styled.h1`
  font-size: 60px;
  white-space: pre-wrap;
`
const Desc = styled.p`
  margin: 50px 100px 50px 0px;
  font-size: 20px;
  font-weight: 500;
  white-space: pre-wrap;
`
const Button = styled(Link)`
  border: none;
  padding: 16px 36px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`;

function Slider() {
  const [slideIndex, setSlideIndex] = useState(0);
  const {profile} = useAuth()

  const handleClick = (direction) => {
    if (direction === "left") {
        setSlideIndex(slideIndex > 0 ? slideIndex - 1 : 2);
    } else {
        setSlideIndex(slideIndex < 2 ? slideIndex + 1 : 0);
    }
  };

  return (
    <Container>
      <Arrow direction="left" onClick={() => handleClick("left")}>
        <ArrowLeftOutlined/>
      </Arrow>
      <Wrapper slideIndex={slideIndex}>
        {sliderItems.map((item) => (
          <Slide bg={item.bg} key={item.id}>
            <ImgContainer>
              <Image src={item.id == 1 ? require("../assets/bed.png") : item.img}/>
            </ImgContainer>
            <InfoContainer>
              <Title>{item.title}</Title>
              <Desc>{item.desc}</Desc>
              {
                profile?.role === 'refugee' ?
                <>
                <Button component={Link} style={{marginRight: '1rem'}} to={'/post/new'}>Add Post</Button>
                <Button component={Link} to={'/viewPosts'}>View Posts</Button>
                </>
                : profile?.role === 'donator' ?
                  <Button component={Link} to={'/explore'}>EXPLORE</Button>
                : <div>You need to log in</div>
              }
            </InfoContainer>
          </Slide>
        ))}
      </Wrapper>
      <Arrow direction="right" onClick={() => handleClick("right")}>
        <ArrowRightOutlined/>
      </Arrow>
    </Container>
  )
}

export default Slider