import React, { useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import Badge from '@mui/material/Badge';
import { Search } from '@material-ui/icons'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import {mobile} from "../responsive"
import { useAuth } from "../contexts/AuthContext"
import { primary, lightGrey, greyMedium, red, blue } from "../constants/colors";
import { BASE_URL } from "../constants/paths";
import Button from '@mui/material/Button';

const Container = styled.div`
  height: 60px;
  border-style: solid;
  border-bottom-width: 1px;
  border-top-width: 0px;
  border-left-width: 0px;
  border-right-width: 0px;
  border-color: ${greyMedium};
  ${mobile({ height: "50px" })}
`

const Wrapper = styled.div`
  padding: 10px 20px;
  display: flex;
  justify-content: space-between;
  ${mobile({ padding: "10px 0px" })}
`

const Left = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
`
const SearchContainer = styled.div`
  border: 1px solid lightgray;
  display: flex;
  align-items: center;
  margin-left: 25px;
  padding: 5px;
`
const Input = styled.input`
  border: none;
  ${mobile({ width: "50px" })}
`
const Center = styled.div`
  flex: 1;
  align-items: center;
  text-align: center;
`
const Logo = styled(Link)`
  font-weight: bold;
  font-size: xx-large;
  color: black;
  ${mobile({ fontSize: "24px" })}
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`
const Right = styled.div`
  flex: 1;
  align-items: center;
  display: flex;
  justify-content: flex-end;
  ${mobile({ flex: 2, justifyContent: "center" })}
`
const MenuItem = styled.div`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  letter-spacing: 1px;
  cursor: pointer;
`
const Image = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  object-fit: cover;
  margin-left: 25px;
  cursor: pointer;
`
const MenuItemWithLink = styled(Link)`
  font-size: 14px;
  cursor: pointer;
  margin-left: 25px;
  letter-spacing: 1px;
  ${mobile({ fontSize: "12px", marginLeft: "10px" })}
  color: #000;
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`
const MenuItemButtonWithLink = styled(Link)`
  border: none;
  padding: 8px 18px;
  background: linear-gradient(
    45deg,
    ${primary},
    #5f70b9
  );
  color: white;
  cursor: pointer;
  border-radius: 186px;
  margin-left: 16px;
  letter-spacing: 1px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`
const SmallMenuItemButtonWithLink = styled(Link)`
  border: none;
  padding: 6px 14px;
  background-color: ${props => props.color};
  color: ${props => props.color === lightGrey ? 'black' : 'white'};
  cursor: pointer;
  border-radius: 50px;
  margin-left: 16px;
  letter-spacing: 1px;
  font-size: small;
  font-weight: 600;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`
const Navbar = () => {
  const { currentUser, profile, logout } = useAuth()
  const navigate = useNavigate()
  const [error, setError] = useState("")
  const [showLogout, setShowLogout] = useState(false)
  const profileImage = profile?.image && profile?.image != "null" ? BASE_URL + '/' + profile?.image : "https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png"

  async function handleLogout() {
    setError("")

    try {
      await logout()
      navigate("/");
    } catch {
      setError("Failed to log out")
    }
  }

  return (
    <Container>
      <Wrapper>
        <Left>
        </Left>
        <Center><Logo component={Link} to={'/'}>Tranood</Logo></Center>
        <Right>
          {currentUser
          ? <>
            {showLogout
            ? <>
              <SmallMenuItemButtonWithLink component={Link} to={'/profile'} color={lightGrey}>View Profile</SmallMenuItemButtonWithLink>
              <SmallMenuItemButtonWithLink component={Link} to={'/profile'} onClick={handleLogout} color={red}>Log Out</SmallMenuItemButtonWithLink>
              </>
            : <MenuItem onClick={() => setShowLogout(!showLogout)}>{currentUser.email}</MenuItem>}
              <Image src={profileImage} onClick={() => setShowLogout(!showLogout)}/>
            </>
          : <><MenuItemWithLink component={Link} to={'/login'}>SIGN IN</MenuItemWithLink>
              <MenuItemButtonWithLink component={Link} to={'/register'}>REGISTER</MenuItemButtonWithLink></>}
          
        </Right>
      </Wrapper>
    </Container>
  )
}

export default Navbar