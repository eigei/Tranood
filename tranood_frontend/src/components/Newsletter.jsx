import { Send } from '@material-ui/icons'
import styled from "styled-components"

const Container = styled.div`
  height: 176px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-bottom: 24px;
`
const Title = styled.h1`
  font-size: 30px;
  margin-bottom: 20px;
`
const InputContainer = styled.div`
  width: 366px;
  height: 40px;
  display: flex;
  justify-content: space-between;
  border: 1px solid lightgray;
`
const Input = styled.input`
  border: none;
  flex: 8;
  padding-left: 20px;
`
const Button = styled.button`
  flex: 1;
  border: none;
  background-color: black;
  color: white;
`

function Newsletter() {
  return (
    <Container>
      <Title>Newsletter</Title>
      <InputContainer>
        <Input placeholder="Enter your email address"/>
        <Button>
          <Send/>
        </Button>
      </InputContainer>
    </Container>
  )
}

export default Newsletter