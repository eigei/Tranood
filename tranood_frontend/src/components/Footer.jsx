import {
  Facebook,
  Instagram,
  MailOutline,
  Phone,
  Room,
  Twitter,
} from "@material-ui/icons";
import styled from "styled-components"
import {mobile} from "../responsive"

const Container = styled.div`
  background-color: black;
`
const Wrapper = styled.div`
  background-color: black;
  padding: 4rem 16rem;
  ${mobile({ padding: "4rem 2rem" })}
`
const FooterHeader = styled.div`
  height: 80px;
  display: flex;
  justify-content: space-between;
`
const Logo = styled.h1`
  font-weight: bold;
  font-size: xx-large;
  color: white;
`
const SocialContainer = styled.div`
  display: flex;
`
const SocialIcon = styled.div`
  width: 40px;
  height: 40px;
  color: white;
  background-color: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 20px;
`
const Divider = styled.hr`
  border-top: 1px solid #f2f2f2;
  border-bottom: 0px;
`
const FooterBody = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: space-between;
  ${mobile({ flexDirection: "column"})}
`
const Column = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  ${mobile({ marginBottom: "16px"})}
`
const Title = styled.span`
  font-size: x-small;
  color: #f2f2f2;
  margin-bottom: 16px;
`
const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`
const ListItem = styled.li`
  margin-bottom: 10px;
  color: white;
`

function Footer() {
  return (
    <Container>
      <Wrapper>
        <FooterHeader>
          <Logo>Tranood</Logo>
          <SocialContainer>
            <SocialIcon color="3B5999">
              <Facebook />
            </SocialIcon>
            <SocialIcon color="E4405F">
              <Instagram />
            </SocialIcon>
            <SocialIcon color="55ACEE">
              <Twitter />
            </SocialIcon>
          </SocialContainer>
        </FooterHeader>
        <Divider/>
        <FooterBody>
          <Column>
            <Title>ABOUT</Title>
            <List>
              <ListItem>Our mission</ListItem>
              <ListItem>News</ListItem>
              <ListItem>Careers</ListItem>
            </List>
          </Column>
          <Column>
            <Title>GET HELP</Title>
            <List>
              <ListItem>Terms & Conditions</ListItem>
              <ListItem>FAQ</ListItem>
            </List>
          </Column>
          <Column>
            <Title>CONTACT</Title>
            <List>
              <ListItem><Room style={{color:"white", fontSize: "14px", marginRight:"10px"}}/>Splaiul Independenței 313, Bucharest, Romania</ListItem>
              <ListItem><Phone style={{color:"white", fontSize: "14px", marginRight:"10px"}}/>+407 123 45 678</ListItem>
              <ListItem><MailOutline style={{color:"white", fontSize: "14px", marginRight:"10px"}}/>admin@tranood.com</ListItem>
            </List>
          </Column>
        </FooterBody>
      </Wrapper>
    </Container>
  )
}

export default Footer