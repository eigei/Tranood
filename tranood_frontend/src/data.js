export const sliderItems = [
{
    id: 1,
    img: "../assets/bed.png",
    title: "SHELTER.\nNOT SHELLING.",
    desc: "Are you a refugee?\nWhether you need a good night's rest on your way to family or got no place to go, we've got you covered. Find free shelter offered by hospitable locals.",
    bg: "e5cdb3",
},
{
    id: 2,
    img: "https://drive.google.com/uc?id=1Gt6jgbdNwc_LeUJHaWrvn91hq2KQN494",
    title: "SHARE A RIDE",
    desc: "Find an empty seat and travel to a safe place.",
    bg: "e7e1e1",
},
{
    id: 3,
    img: "https://drive.google.com/uc?id=1BYhd1-enqKxEKBR6zHKLWY-agmGwGu28",
    title: "EVERYTHING DONATIONS",
    desc: "Food. Clothing. Blankets. Toys. Anything that helps. Browse available items up for donation or request exactly what you need.",
    bg: "FFF",
},
]
