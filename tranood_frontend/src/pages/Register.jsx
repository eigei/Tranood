import styled from 'styled-components'
import { mobile } from '../responsive';
import React, { useRef, useState } from "react"
import { useAuth } from '../contexts/AuthContext'
import { Link, useNavigate } from "react-router-dom"
import axios from 'axios';
import { API_URL } from '../constants/paths';
import { useTogglePasswordVisibility } from "../hooks/useTogglePasswordVisibility"
import { MailOutlined, LockOutlined, VisibilityOutlined, VisibilityOffOutlined } from "@material-ui/icons";
import BadgeOutlinedIcon from '@mui/icons-material/BadgeOutlined';
import VolunteerActivismOutlinedIcon from '@mui/icons-material/VolunteerActivismOutlined';
import LuggageOutlinedIcon from '@mui/icons-material/LuggageOutlined';
import { primary, greyMedium, lightGrey } from '../constants/colors';
import {fetchSignInMethodsForEmail} from 'firebase/auth';
import { auth } from '../firebase';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://static.vecteezy.com/system/resources/previews/002/038/461/non_2x/calm-mountain-forest-landscape-illustration-vector.jpg")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: white;
  border-radius: 16px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  ${mobile({ width: "75%" })};
`;

const Title = styled.div`
  font-size: 24px;
  font-weight: 700;
  margin-bottom: 14px;
`;

const Subtitle = styled.h2`
  font-size: 18px;
  font-weight: 100;
`;

const SelectionContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 56px 16px;
  justify-content: space-between;
`;

const SelectionBox = styled.div`
  width: 45%;
  padding: 16px;
  background-color: ${lightGrey};
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border-width: 1px;
  border-color: ${greyMedium};
  border-style: solid;
  cursor: pointer;
  border-radius: 16px;
  ${({ selected }) => selected && `
    -webkit-box-shadow: 1px 1px 3px 1px #FF8080, -2px 1px 3px 1px #FFE488, -1px -1px 3px 1px #8CFF85, 2px -1px 3px 1px #80C7FF, 2px 2px 3px 1px #E488FF, -2px 2px 3px 1px #FF616B, -2px -1px 5px 1px #8E5CFF, 1px 1px 3px 1px rgba(0,0,0,0);
    box-shadow: 1px 1px 3px 1px #FF8080, -2px 1px 3px 1px #FFE488, -1px -1px 3px 1px #8CFF85, 2px -1px 3px 1px #80C7FF, 2px 2px 3px 1px #E488FF, -2px 2px 3px 1px #FF616B, -2px -1px 5px 1px #8E5CFF, 1px 1px 3px 1px rgba(0,0,0,0);
  `}
`;

const SelectionTitle = styled.p`
  font-size: 18px;
  font-weight: 500;
  margin-top: 16px;
  margin-bottom: 8px;
`;

const SelectionSubtitle = styled.span`
  font-size: 12px;
  font-weight: 100;
  text-align: center;
`;

const Space = styled.div`
  padding: 10px;
`

const Form = styled.form`
  display: flex;
  flex-direction: column;
  padding: 20px 36px;
`;

const InputView = styled.div`
  margin-top: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding: 0 10px;
  border-color: ${greyMedium};
  border-style: solid;
  border-bottom-width: 1px;
  border-top-width: 0;
  border-left-width: 0;
  border-right-width: 0;
`

const Input = styled.input`
  flex: 1;
  border-color: ${greyMedium};
  padding: 10px;
  border: 0;
  &:focus, &:visited, &:hover, &:link, &:active {
    outline: none;
  }
`;

const Agreement = styled.span`
  font-size: 12px;
  margin-top: 24px;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const Button = styled.button`
  border: none;
  padding: 16px 36px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  margin-top: 36px;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
`;

const BackButton = styled.button`
  border: none;
  padding: 16px 36px;
  background-color: ${greyMedium};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  margin-top: 36px;
  border-radius: 36px;
  &:disabled{
    color: #000;
    cursor: not-allowed;
  }
`;

const Alert = styled.div`
  color: #F00;
`;

const AccountQuestion = styled.div`
  font-size: 12px;
`
const Logo = styled(Link)`
  font-weight: bold;
  font-size: xx-large;
  color: #fefefe;
  position: absolute;
  top: 16px;
  ${mobile({ fontSize: "24px" })}
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`

const Register = () => {
  const nameRef = useRef()
  const emailRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const { signup } = useAuth()
  const [error, setError] = useState("")
  const [type, setType] = useState("refugee")
  const [step, setStep] = useState(0)
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()
  const { passwordVisibility: passwordVisibility, handlePasswordVisibility: handlePasswordVisibility } =
    useTogglePasswordVisibility();
  const { passwordVisibility: confirmPasswordVisibility, handlePasswordVisibility: handleConfirmPasswordVisibility } =
    useTogglePasswordVisibility();

  async function handleSubmit(e) {
    e.preventDefault()

    if (nameRef.current.value === "") {
      return setError("Please enter a name")
    }

    const list = await fetchSignInMethodsForEmail(auth, emailRef.current.value)
    if (list.length !== 0) {
      return setError("Email already used")
    }

    if (passwordRef.current.value === "") {
      return setError("Please enter a password")
    }

    if (passwordConfirmRef.current.value === "") {
      return setError("Please enter confirm password")
    }

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match")
    }

    try {
      setError("")
      setLoading(true)
      const {user} = await signup(emailRef.current.value, passwordRef.current.value)
      await axios.post(`${API_URL}/auth/register`, {
        name: nameRef.current.value,
        email: emailRef.current.value,
        role: type,
        firebase_uid: user.uid,
      }).then(function () {
        navigate("/")
      })
      .catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.log(error);
      setError(error)
    }

    setLoading(false)
  }

  return (
    <Container>
      <Logo component={Link} to={'/'}>Tranood</Logo>
      <Wrapper>
        <Title>CREATE AN ACCOUNT</Title>
        {step === 0 &&
        <>
        <Subtitle>Select account type</Subtitle>
        <SelectionContainer>
          <SelectionBox selected={type === "refugee"} onClick={() => setType("refugee")}>
            <LuggageOutlinedIcon sx={{ color: greyMedium, fontSize: 40, padding: 3 }}/>
            <SelectionTitle>REFUGEE</SelectionTitle>
            <SelectionSubtitle>Find or request anything you need including food, transport and shelter.</SelectionSubtitle>
          </SelectionBox>
          <Space/>
          <SelectionBox selected={type === "donator"} onClick={() => setType("donator")}>
            <VolunteerActivismOutlinedIcon sx={{ color: greyMedium, fontSize: 40, padding: 3 }}/>
            <SelectionTitle>DONATOR</SelectionTitle>
            <SelectionSubtitle>Fulfill refugee needs or post what you can help with.</SelectionSubtitle>
          </SelectionBox>
        </SelectionContainer>
        <Button onClick={() => setStep(step+1)}>CONTINUE</Button>
        </>
        }
        {step === 1 &&
        <>
        <Subtitle>Enter details for a {type} account</Subtitle>
        {error && <Alert variant="danger">{error}</Alert>}
        <Form>
          <InputView>
            <BadgeOutlinedIcon style={{color: greyMedium}}/>
            <Input placeholder="Name" ref={nameRef} />
          </InputView>
          <InputView>
            <MailOutlined style={{color: greyMedium}}/>
            <Input placeholder="Email" ref={emailRef} />
          </InputView>
          <InputView>
            <LockOutlined style={{color: greyMedium}}/>
            <Input
              placeholder="Password"
              type={passwordVisibility}
              ref={passwordRef}
            />
            {passwordVisibility === "password"
              ? <VisibilityOutlined style={{color: greyMedium}} onClick={handlePasswordVisibility}/>
              : <VisibilityOffOutlined style={{color: greyMedium}} onClick={handlePasswordVisibility}/>
            }
          </InputView>
          <InputView>
            <LockOutlined style={{color: greyMedium}}/>
            <Input
              placeholder="Confirm Password"
              type={confirmPasswordVisibility}
              ref={passwordConfirmRef}
            />
            {confirmPasswordVisibility === "password"
              ? <VisibilityOutlined style={{color: greyMedium}} onClick={handleConfirmPasswordVisibility}/>
              : <VisibilityOffOutlined style={{color: greyMedium}} onClick={handleConfirmPasswordVisibility}/>
            }
          </InputView>
          
          <ButtonContainer>
            <BackButton disabled={loading} onClick={() => setStep(step-1)}>BACK</BackButton>
            <Button disabled={loading} onClick={handleSubmit}>CREATE</Button>
          </ButtonContainer>
          <Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>Privacy Policy</b>
          </Agreement>
        </Form>
        </>
        }
        <AccountQuestion>
          Already have an account? <Link to="/login">Sign In</Link>
        </AccountQuestion> 
      </Wrapper>
    </Container>
  )
}

export default Register