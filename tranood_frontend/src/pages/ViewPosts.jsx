import { useEffect, useState, Fragment } from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { publicRequest } from "../requestMethods";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import "../css/styles.css";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Box from "@mui/material/Box";
import Button from "@material-ui/core/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import { useAuth } from "../contexts/AuthContext";
import axios from "axios";
import { API_URL } from "../constants/paths";
import { TextField } from "@material-ui/core";

const Container = styled.div``;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Title = styled.div`
  padding: 16px;
  font-size: 24px;
  font-weight: 800;
  align-self: center;
`;

const ViewPosts = () => {
  const [donatorPosts, setDonatorPosts] = useState([]);
  const [expandedId, setExpandedId] = useState(-1);
  const [allUsers, setAllUsers] = useState([]);
  const { currentUser } = useAuth();

  const handleExpandClick = (i) => {
    setExpandedId(expandedId === i ? -1 : i);
  };

  const getAllUsers = async () => {
    try {
      await axios
        .get(`${API_URL}/users`)
        .then((res) => {
          setAllUsers(res.data);
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  }

  const getDonatorName = donatorId => {
    for (let i = 0; i < allUsers.length; i++) {
      if(allUsers[i].firebase_uid === donatorId) {
        return allUsers[i].name;
      }
    }
    return "Unknown"
  }

  const getDonatorPosts = async () => {
    try {
      await axios
        .get(`${API_URL}/posts/items/user/${currentUser.uid}?active=true`)
        .then((res) => {
          setDonatorPosts(res.data);
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  };

  const handleAcceptRequest = async (itemId, donatorId) => {
    try {
      let promptResp = prompt("Do you want to add comments?", "No comments");
      let comment;
      if (promptResp === null || promptResp === "" ) {
        comment = "No comments";
      } else {
        comment = promptResp;
      }
      console.log(itemId);
      console.log(comment)
      const body = {};
      body["comment"] = comment;
      const token = await currentUser.getIdToken();
      const headers = {};
      headers["headers"] = { Authorization: `Bearer ${token}` };
      await axios
        .put(`${API_URL}/items/${itemId}/accept/${donatorId}`, body, headers)
        .then((res) => {
          getDonatorPosts();
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  };

  const handleInactiveButton = async (itemId) => {
    try {
      const body = {};
      body["active"] = false;
      const token = await currentUser.getIdToken();
      const headers = {};
      headers["headers"] = { Authorization: `Bearer ${token}` };
      await axios
        .patch(`${API_URL}/items/${itemId}`, body, headers)
        .then((res) => {
          getDonatorPosts();
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  };

  const bull = (
    <Box
      component="span"
      sx={{ display: "inline-block", mx: "2px", transform: "scale(0.8)" }}
    >
      •
    </Box>
  );

  useEffect(() => {
    getDonatorPosts();
    getAllUsers();
  }, []);

  return (
    <Container>
      <Navbar />
      <Row>
        <Title>View Active Posts</Title>
      </Row>
      <div className="posts">
        {donatorPosts.map((x, i) => {
          return (
            <Card
              key={i}
              className="postCard"
              variant="outlined"
              sx={{ minWidth: 275 }}
            >
              <CardContent>
                <CardActions disableSpacing>
                  <Typography variant="h5" component="div">
                    Post {i + 1}
                  </Typography>

                  <IconButton
                    aria-label="show more"
                    onClick={() => handleExpandClick(i)}
                    aria-expanded={expandedId === i}
                  >
                    <ExpandMoreIcon />
                  </IconButton>
                </CardActions>

                {x.items.map((y, j) => {
                  return (
                    <Collapse
                      key={j}
                      in={expandedId === i}
                      timeout="auto"
                      unmountOnExit
                    >
                      <Card
                        className="itemCard"
                        variant="outlined"
                        sx={{ minWidth: 275 }}
                        style={{ opacity: y.active ? 1.0 : 0.5 }}
                      >
                        <CardContent>
                          <Typography variant="h5" component="div">
                            Item {j + 1}
                          </Typography>
                          {y.name} {bull}
                          {y.tag}
                          {bull}
                          <br />
                          <br />
                          <Typography sx={{ mb: 1.5 }} color="text.secondary">
                            {y.description}
                          </Typography>
                          {y.requested.length > 0 && (
                            <div>
                              Donators:
                              <List
                                sx={{
                                  width: "100%",
                                  maxWidth: 360
                                }}
                              >
                                {y.requested.map((z, k) => {
                                  return (
                                    <div key={k}>
                                      <ListItem
                                        alignItems="flex-start"
                                        style={{ cursor: y.active ? 'pointer' : undefined}}
                                        className="donators"
                                        onClick={y.active ? () =>
                                          handleAcceptRequest(y._id, z) : undefined
                                        }
                                      >
                                        <ListItemAvatar>
                                          <Avatar alt="Test" />
                                        </ListItemAvatar>
                                        <ListItemText
                                          primary={getDonatorName(z)}
                                          secondary={
                                            <>
                                              <Typography
                                                sx={{ display: "inline" }}
                                                component="span"
                                                variant="body2"
                                                color="text.primary"
                                              >
                                                Click to accept
                                              </Typography>
                                            </>
                                          }
                                        />
                                      </ListItem>
                                    </div>
                                  );
                                })}
                              </List>
                            </div>
                          )}
                        </CardContent>
                        <CardActions>
                          {y.active === true ? (
                            <Button
                              size="small"
                              variant="contained"
                              className="inactiveButton"
                              onClick={() => handleInactiveButton(y._id)}
                            >
                              Make inactive
                            </Button>
                          ) : (
                            <Typography
                              className="alreadyInactive"
                              sx={{ mb: 1.5 }}
                              color="text.secondary"
                            >
                              INACTIVE
                            </Typography>
                          )}
                        </CardActions>
                      </Card>
                    </Collapse>
                  );
                })}
              </CardContent>
            </Card>
          );
        })}
      </div>
      <Footer />
    </Container>
  );
};

export default ViewPosts;
