import styled from 'styled-components'
import { ToastContainer, toast } from "react-toastify";
import Navbar from "../components/Navbar"
import Footer from "../components/Footer"
import { useState } from 'react';
import { useAuth } from "../contexts/AuthContext"
import { Link } from "react-router-dom"
import { primary, greyMedium, lightGrey, red } from '../constants/colors';
import { API_URL } from "../constants/paths";
import axios from "axios";

const Container = styled.div`
`

const Wrapper = styled.div`
  padding: 16px;
  display: flex;
  flex-wrap: wrap;
`;

const Card = styled.div`
  width: 25%;
  padding: 20px;
  margin: 24px 16px;
  background-color: ${lightGrey};
  border-radius: 16px;
  border-style: solid;
  border-color: ${greyMedium};
  border-right-width: 2px;
  border-left-width: 2px;
  border-top-width: 2px;
  border-bottom-width: 2px;
`

const Form = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 18px;
`;

const Label = styled.div`
  font-size: 16px;
  font-weight: 800;
  margin-bottom: 4px;
`
const Input = styled.input`
  width: 94%;
  padding: 8px 8px;
  margin-bottom: 16px;
`
const Select = styled.select`
  padding: 8px 8px;
  margin-bottom: 16px;
`
const TextArea = styled.textarea`
  width: 94%;
  padding: 8px 8px;
  margin-bottom: 36px;
  resize: vertical;
`
const PostButton = styled.button`
  border: none;
  margin: 16px;
  padding: 16px 36px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
`
const Button = styled.button`
  border: none;
  padding: 16px 36px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  margin-top: 18px;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
`;
const RemoveButton = styled.button`
  border: none;
  padding: 16px 36px;
  background-color: ${red};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  margin-top: 18px;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
`
const Row = styled.div`
  display: flex;
  justify-content: space-between;
`
const Title = styled.div`
  padding: 16px;
  font-size: 24px;
  font-weight: 800;
  align-self: center;
`

const AddPost = () => {
  const [items, setItems] = useState([{ "name": "", "tag": "shelter", "description": ""}])
  const {currentUser} = useAuth()

  const validateItems = () => {
		let res = {pos: -1, field: ''};
		for (let i = 0; i < items.length; i++) {
				if (items[i]["name"] === "") {
					res.pos = i;
					res.field = 'name';
					break;
				} else if (items[i]["tag"] === "") {
					res.pos = i;
					res.field = 'tag';
					break;
				}
		}
		return res;
	}

	const handlePost = async (event) => {
    event.preventDefault();
		const res = validateItems();
		if (res.pos !== -1) {
			const errorMessage = "Please enter a name for item " + (res.pos + 1);
      toast.error(errorMessage, {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
			return;
		}
		const body = {};
    body["items"] = items;
		const token = await currentUser.getIdToken();
		await axios.post(`${API_URL}/posts`, body,
    {
			headers: {
				'Authorization' : `Bearer ${token}`

			}
		}).then(res => {
			console.log(res);
      toast.success('Post successful', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
		}).catch(err => {
			console.log("error", err.message)
		})
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...items];
    list[index][name] = value;
    setItems(list);
  };

  const handleRemoveClick = index => {
    const list = [...items];
    list.splice(index, 1);
    setItems(list);
  };

	const handleAddClick = () => {
		setItems([...items, { "name": "", "tag": "shelter", "description": "" }])
	}

  return (
    <Container>
      <Navbar/>
      <Row>
      <Title>New Post</Title>
      <PostButton onClick={handlePost}>POST</PostButton>
      </Row>
      <Wrapper>
        {items.map((x, i) => {
          return (
            <Card key={i}>
            <Form>
              <Label>Name</Label>
              <Input name="name" placeholder="Name" value={x["name"]} onChange={e => handleInputChange(e, i)}></Input>
              <Label>Tag</Label>
              <Select name="tag" id="tag" value={x["tag"]} onChange={e => handleInputChange(e, i)}>
                <option value="shelter">Shelter</option>
                <option value="transport">Transport</option>
                <option value="donation">Donation</option>
              </Select>
              <Label>Description</Label>
              <TextArea name="description" placeholder="Description" rows="3"  value={x["description"]} onChange={e => handleInputChange(e, i)}></TextArea>
              {items.length !== 1 && <RemoveButton onClick={() => handleRemoveClick(i)}>REMOVE ITEM</RemoveButton>}
              {items.length - 1 === i && <Button onClick={handleAddClick}>ADD ITEM</Button>}
            </Form>
            </Card>
          )
        })}
      </Wrapper>
      <Footer/>
      <Link to="/viewPosts">
        <ToastContainer
          position="top-right"
          autoClose={2000}
          containerId="error"
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </Link>
    </Container>
  )
}

export default AddPost