import { useRef, useState } from "react"
import { Add, Remove } from "@material-ui/icons";
import { Link, useLocation } from "react-router-dom";
import styled from "styled-components"
import Announcement from "../components/Announcement"
import Footer from "../components/Footer"
import Navbar from "../components/Navbar"
import Newsletter from "../components/Newsletter"
import { publicRequest } from "../requestMethods";
import { mobile } from "../responsive";
import { useDispatch } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import { useAuth } from "../contexts/AuthContext";
import { primary } from "../constants/colors";
import axios from "axios";
import { BASE_URL, API_URL } from "../constants/paths";
import { CloudUploadOutlined } from "@material-ui/icons"
import { greyMedium } from '../constants/colors';

import 'react-toastify/dist/ReactToastify.css';

const Container = styled.div``
const Wrapper = styled.div`
  padding: 50px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
const Info = styled.div`
  opacity: 0;
  width: 128px;
  height: 128px;
  border-radius: 50%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.2);
  z-index: 3;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: all 0.5s ease;
`
const ImageWrapper = styled.div`
  width: 128px;
  height: 128px;
  border-radius: 50%;
  object-fit: cover;
  position: relative;
  padding: 2px;
  border-style: solid;
  border-width: 2px;
  border-color: ${greyMedium};
  &:hover ${Info}{
    opacity: 1;
  }
`
const Icon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px;
  transition: all 0.5s ease;
  &:hover {
    background-color: #e9f5f5;
    transform: scale(1.1);
  }
`
const Image = styled.img`
  width: 128px;
  height: 128px;
  border-radius: 50%;
  object-fit: cover;
`
const Name = styled.input`
  font-weight: 200;
  font-size: 24px;
  width: 200px;
  background-color: #FFF;
  color: #000;
  text-align: center;
  ${({ disabled }) => disabled
  ? `
    border: none;
    padding-left: 8px;
    padding-right: 8px;
    padding-top 8px;
    padding-bottom: 9px;
    margin: 36px 0px;
  `
  : `
    text-align: center;
    border-color: ${greyMedium};
    border-bottom-width: 1px;
    border-top-width: 0;
    border-left-width: 0;
    border-right-width: 0;
    padding: 8px 8px;
    margin: 36px 0px;
  `}
  &:focus, &:visited, &:hover, &:link, &:active {
    outline: none;
  }
`
const Desc = styled.p`
  margin: 20px 0px;
`
const Price = styled.span`
  font-weight: 100;
  font-size: 40px;
`
const FilterContainer = styled.div`
  width: 50%;
  margin: 30px 0px;
  display: flex;
  justify-content: space-between;
  align-content: center;
  ${mobile({ width: "100%" })};
`
const Filter = styled.div`
  display: flex;
  align-items: center;
`
const FilterTitle = styled.div`
  display: flex;
  align-items: center;
`
const FilterColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  border-style: ${(props) => props.color === props.selectedColor && "solid"};
  background-color: ${(props) => props.color};
  margin: 0px 5px;
  cursor: pointer;
`
const FilterSelect = styled.select`
  margin-left: 10px;
  padding: 5px;
  cursor: pointer;
`
const FilterOption = styled.option``;
const AddContainer = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${mobile({ width: "100%" })};
`
const AmountContainer = styled.div`
  display: flex;
  align-items: center;
  font-weight: 700;
`
const Amount = styled.span`
  width: 30px;
  height: 30px;
  border-radius: 10px;
  border: 1px solid #085ca4;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0px 5px;
`
const Button = styled.button`
  border: none;
  padding: 16px 36px;
  width: 186px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
  ${({ isEditing }) => isEditing && `
    background: linear-gradient(
      45deg,
      ${primary},
      #5f70b9
    );
  `}
`

const Profile = () => {
  const [file, setFile] = useState(null);
  const { currentUser, profile, logout } = useAuth()
  const hiddenFileInput = useRef(null);
  const handleImageClick = event => {
    hiddenFileInput.current.click();
  };
  const [isEditing, setIsEditing] = useState(false);
  const [name, setName] = useState(profile?.name);
  const initialImage = profile?.image && profile?.image != "null" ? BASE_URL + '/' + profile?.image : "https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png"
  const [imagePreviewUrl, setImagePreviewUrl] = useState(initialImage);

  const handleClick = async () => {
    if (isEditing) {
      var token = await currentUser.getIdToken();
      console.log(token);
      const formData = new FormData()
      let formDataSize = 0;
      if (name !== profile?.name) {
        formData.append("name", name);
        formDataSize++;
      }
      if (file) {
        formData.append("image", file);
        formDataSize++;
      }
      if (formDataSize > 0) {
        axios.patch(`${API_URL}/users`, formData,
        {
          headers: {
            'Authorization' : `Bearer ${token}`
          }
        }).then(res => {
          console.log(res.data)
        }).catch(err => {
          console.log("error", err.message)
        })
      }
    }
    setIsEditing(!isEditing);
  }

  const photoUpload = e => {
    e.preventDefault();
    const reader = new FileReader();
    const chosenFile = e.target.files[0];
    reader.onloadend = () => {
      setFile(chosenFile);
      setImagePreviewUrl(reader.result);
    }
    reader.readAsDataURL(chosenFile);
  }

  return (
    <Container>
      <Navbar/>
      <Wrapper>
        {isEditing
        ? <>
          <ImageWrapper onClick={handleImageClick}>
          <Image src={imagePreviewUrl}/>
          <Info>
            <Icon>
            <CloudUploadOutlined/>
            </Icon>          
          </Info>
          </ImageWrapper>
          <Name type="text" value={name} onChange={e => setName(e.target.value)}/>
          </>
        : <>
          <Image src={imagePreviewUrl} style={{padding: 4}}/>
          <Name value={name} disabled/>
          </>}
        <input
        type="file"
        ref={hiddenFileInput}
        onChange={photoUpload}
        style={{display: 'none'}}/>
        <Button onClick={() => handleClick()} isEditing={isEditing}>{isEditing ? "SAVE" : "EDIT PROFILE"}</Button>
        <Desc>{profile?.role}</Desc>
      </Wrapper>
      <Newsletter/>
      <Footer/>
    </Container>
  )
}

export default Profile