import Navbar from "../components/Navbar"
import Footer from "../components/Footer"
import styled from 'styled-components'

const Container = styled.div`
`
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
const Text = styled.p`
  width: 50vw;
  margin: 16px;
  font-weight: 100;
  white-space: pre-wrap;
`
const Image = styled.img`
  width: 50vw;
  margin: 32px;
  border-radius: 16px;
  align-self: center;
  object-fit: cover;
  cursor: pointer;
`
const text = "On February 24th, Ukrainians all over the country woke up at 5 AM from a call, asking: \“Are there explosions in your city\”? This is how the aggressive war held by Russia started. What earlier had been just a nightmare became a reality for millions of people.\n\nBut Ukraine is a country of strong people ready to stand for their freedom. From Ukrainian soldiers to simple families, everyone is united under one purpose – win and build even a stronger nation together!\n\nAnd everyone dreams of waking up one morning from a call, saying, “We have won!”\n\nSince day one, Tranood has pledged to help any refugee in need. Today, we continue to make no execption and are grateful for all the kind donators. They don't just represent all the available shelter locations, transports and donations. They represent a warm heart to a free world.\n\n However, if you can't help a refugee in person through our platform, we suggest donating to a range of organizations:\n\n"

const SupportUkraine = () => {
  return (
    <Container>
      <Navbar/>
      <Wrapper>
        <Image src={require("../assets/ukraine_no_war.png")} alt="No War!"/>
        <Text>{text}<p><a href="https://www.airbnb.org/" target="_blank" rel="nofollow noopener"><strong>Airbnb.org</strong></a>. Airbnb's nonprofit is asking people — especially those residing in European nations near Ukraine — to <a href="https://www.airbnb.org/help-ukraine" target="_blank" rel="nofollow noopener">sign up</a> to provide temporary housing for Ukrainian refugees or&nbsp;<a href="https://www.airbnb.org/get-involved" target="_blank" rel="nofollow noopener">donate</a> to their stays. The San Francisco-based company is also <span class="link"><a href="https://www.cbsnews.com/news/russia-ukraine-war-refugees-airbnb/" target="_blank" data-invalid-url-rewritten-http="">pledging to shelter up to 100,000</a></span> fleeing Ukraine.</p>
        <br/><p><a href="https://www.care.org/" target="_blank" rel="nofollow noopener"><strong>CARE</strong></a><strong>. </strong>The international humanitarian group is providing food, water and other items to families fleeing violence in Ukraine. Contribute <a href="https://my.care.org/site/Donation2?df_id=31067&amp;mfc_pref=T&amp;31067.donation=form1&amp;s_src=172223UCF000&amp;_ga=2.135172726.651260027.1646165065-461925101.1646165065" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://convoyofhope.org/response-updates/convoy-reaching-out-to-help-in-ukrainian-crisis/" target="_blank" rel="nofollow noopener"><strong>Convoy of Hope</strong></a>. The disaster relief group says it's partnering with a local Polish organization to provide meals to refugees entering Poland, as well as to deliver food, water and other basics across the region. Donate <a href="https://convoyofhope.org/donate/" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.coreresponse.org/ukraine-refugee-crisis-relief-poland" target="_blank" rel="nofollow noopener"><strong>CORE</strong></a>. The emergency response nonprofit is on the ground in Poland, distributing hygiene kits and thermal blankets, as well as emergency cash assistance to help families resettle. Contribute <a href="https://donate.coreresponse.org/give/393950/#!/donation/checkout" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.doctorswithoutborders.org/what-we-do/news-stories/news/msf-assesses-response-ukraine-conflict-escalates" target="_blank" rel="nofollow noopener"><strong>Doctors Without Borders</strong></a><strong>.</strong>&nbsp;Staffers with the medical relief organization remain in Ukraine and are "seeking ways to respond to the medical and humanitarian needs as the conflict evolves." Offer support <a href="https://donate.doctorswithoutborders.org/secure/donate?_ga=2.101951365.5685454.1646166587-59975809.1646166587" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.icrc.org/en/where-we-work/europe-central-asia/ukraine" target="_blank" rel="nofollow noopener"><strong>International Committee of the Red Cross</strong></a><strong>.</strong> The Swiss-based organization is supporting the work of the Ukrainian Red Cross in helping those impacted by the war. <a href="https://www.icrc.org/en/donate/ukraine-b" target="_blank" rel="nofollow noopener">Donate to the ICRC</a>.</p>
        <br/><p><a href="https://internationalmedicalcorps.org/who-we-are/" target="_blank" rel="nofollow noopener"><strong>International Medical Corps</strong></a><strong>.</strong> The first responders' organization has teams inside Ukraine and in the surrounding regions to offer medical and mental health services. Link to <a href="https://give.internationalmedicalcorps.org/page/99837/donate/1?ea.tracking.id=DP~UA22~DPLBU2202&amp;_ga=2.91192156.797875621.1646669875-1079461136.1646669875" target="_blank" rel="nofollow noopener">contribute.</a></p>
        <br/><p><a href="https://internews.org/about/about-us/" target="_blank" rel="nofollow noopener"><strong>Internews</strong></a><strong>.</strong> Supports independent media and at-risk journalists in 100 countries, with regional hubs including in Kyiv. Donate <a href="https://internews.org/donate/" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.gofundme.com/f/keep-ukraines-media-going" target="_blank" rel="nofollow noopener"><strong>"Keep Ukraine's media going"</strong></a><strong>&nbsp; </strong>is a GoFundMe campaign for journalists around Ukraine that also aims to help reporters relocate and continue their work from neighboring countries. Donations can be made <a href="https://www.gofundme.com/f/keep-ukraines-media-going/donate" target="_blank" rel="nofollow noopener">here</a>.&nbsp;</p>
        <br/><p><a href="https://kyivindependent.com/" target="_blank" rel="nofollow noopener"><strong>Kyiv Independent</strong></a><strong>.</strong> The English-language news site has launched a GoFundMe campaign asking for <a href="https://www.gofundme.com/f/kyivindependent-launch" target="_blank" rel="nofollow noopener">support</a>.</p>
        <br/><p>The&nbsp;<a href="https://762project.org/en/" target="_blank" rel="nofollow noopener"><strong>762 Project</strong></a><strong>.&nbsp;</strong>Volunteers have been collecting, analyzing and posting information about Russia's troop buildup along Ukraine's border for much of the past year. Support the project&nbsp;<a href="https://762project.org/en/support-the-project/" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.projecthope.org/" target="_blank" rel="nofollow noopener"><strong>Project Hope</strong></a><strong>.</strong> The global health and humanitarian relief organization is on the ground in and around Ukraine delivering medicines and medical supplies. It has teams in Ukraine, Poland, Moldova and Romania. Donate <a href="https://secure.projecthope.org/site/SPageNavigator/2022_02_Ukraine_Response_Web_UNR.html&amp;s_subsrc=bt1" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://razomforukraine.org/" target="_blank" rel="nofollow noopener"><strong>Razom for Ukraine</strong></a><strong>. </strong>Started in 2014 and devoted to building a stronger democracy in Ukraine, the nonprofit is now "focused on purchasing medical supplies for critical situations like blood loss and other tactical medicine items." Here's a <a href="https://razomforukraine.org/the-first-urgent-shipment/" target="_blank" rel="nofollow noopener">list</a> of supplies it has purchased already and an appeal for more&nbsp;<a href="https://razomforukraine.org/donate/" target="_blank" rel="nofollow noopener">support</a>.&nbsp;</p>
        <br/><p><a href="https://www.facebook.com/sunflowerofpeace/" target="_blank" rel="nofollow noopener"><strong>Sunflower of Peace</strong></a><strong>.</strong> The nonprofit's current mission involves providing medical and humanitarian aid to people impacted by violence in Ukraine. It's accepting donations through its <a href="https://www.facebook.com/donate/507886070680475/2106406599536923" target="_blank" rel="nofollow noopener">Facebook</a> page.&nbsp;</p>
        <br/><p><a href="https://www.unicef.org/what-we-do" target="_blank" rel="nofollow noopener"><strong>UNICEF</strong></a><strong>. </strong>The global group devoted to safeguarding children is working to provide humanitarian supplies to families without safe water or electricity due to the conflict. Contributions can be made <a href="https://www.unicef.org/emergencies/conflict-ukraine-pose-immediate-threat-children?utm_source=twitter&amp;utm_medium=organic-en&amp;utm_content=ukraine_crisis&amp;utm_campaign=ukraine_crisis" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://www.unitedway.org/" target="_blank" rel="nofollow noopener"><strong>United Way Worldwide</strong></a>. The world's biggest privately funded nonprofit has set up a relief fund in response to the intensifying humanitarian crisis to provide transportation, shelter, food and medicine, including infant supplies such as baby formula. Donations to the United for Ukraine Fund can be made <a href="https://www.unitedway.org/our-impact/work/no-nav/unitedforukraine/" target="_blank" rel="nofollow noopener">here</a>.</p>
        <br/><p><a href="https://voices.org.ua/en/" target="_blank" rel="nofollow noopener"><strong>Voices of Children</strong></a><strong>.</strong> The Ukraine-based charitable foundation has been offering psychological counseling, including art therapy, for children affected by war in the country's east since 2015, according to its site. The group is currently helping children and families across Ukraine, including helping with evacuations.</p>
        <br/><p><a href="https://wck.org/" target="_blank" rel="nofollow noopener"><strong>World Central Kitchen</strong></a><strong>.</strong>&nbsp;Founded by world-renowned chef José Andrés after an earthquake devastated Haiti in 2010, the group provides meals to people in need around the globe. Andrés is now&nbsp;<span class="link"><a href="https://www.cbsnews.com/news/jose-andres-ukraine-refugees-poland/" target="_blank" data-invalid-url-rewritten-http="">feeding Ukrainian refugees at the Polish border.</a></span>&nbsp;Those wanting to help finance the group can do so&nbsp;<a href="https://donate.wck.org/give/236738/#!/donation/checkout" target="_blank" rel="nofollow noopener">here</a>.&nbsp;</p>
        <br/>
        </Text>
      </Wrapper>
      
      <Footer/>
    </Container>
  )
}

export default SupportUkraine