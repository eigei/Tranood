import { useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components'
import { login } from '../redux/apiCalls';
import { mobile } from '../responsive';
import { useAuth } from "../contexts/AuthContext"
import { Link, useNavigate } from "react-router-dom"
import { useTogglePasswordVisibility } from "../hooks/useTogglePasswordVisibility"
import { MailOutlined, LockOutlined, VisibilityOutlined, VisibilityOffOutlined } from "@material-ui/icons";
import { primary, greyMedium } from '../constants/colors';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://static.vecteezy.com/system/resources/previews/002/773/418/original/gradient-calming-purple-tropical-background-free-vector.jpg")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 25%;
  padding: 20px;
  background-color: white;
  border-radius: 16px;
  ${mobile({ width: "75%" })};
`;

const Title = styled.div`
  font-size: 24px;
  font-weight: 700;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 10px 36px;
`;

const InputView = styled.div`
  margin: 10px 0px;
  margin-top: 24px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding: 0 10px;
  border-color: ${greyMedium};
  border-style: solid;
  border-bottom-width: 1px;
  border-top-width: 0;
  border-left-width: 0;
  border-right-width: 0;
`

const Input = styled.input`
  width: 100%;
  border-color: ${greyMedium};
  padding: 10px;
  border: 0;
  &:focus, &:visited, &:hover, &:link, &:active {
    outline: none;
  }
`;

const Button = styled.button`
  border: none;
  padding: 16px 36px;
  background-color: ${primary};
  letter-spacing: 2px;
  font-weight: 700;
  color: white;
  cursor: pointer;
  margin-bottom: 10px;
  margin-top: 54px;
  border-radius: 36px;
  &:disabled{
    background-color: #990099;
    cursor: not-allowed;
  }
`;

const StyledLink = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

const Alert = styled.span`
  color: red;
`;

const AccountQuestion = styled.div`
  margin-top: 12px;
  font-size: 12px;
`
const Logo = styled(Link)`
  font-weight: bold;
  font-size: xx-large;
  color: #fefefe;
  position: absolute;
  top: 16px;
  ${mobile({ fontSize: "24px" })}
  &:focus, &:visited, &:hover, &:link, &:active {
    text-decoration: none;
  }
`

const Login = () => {
  
  const emailRef = useRef()
  const passwordRef = useRef()
  const { login } = useAuth()
  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()
  const { passwordVisibility, handlePasswordVisibility } =
    useTogglePasswordVisibility();

  async function handleSubmit(e) {
    e.preventDefault()

    try {
      setError("")
      setLoading(true)
      await login(emailRef.current.value, passwordRef.current.value)
      navigate("/")
    } catch {
      setError("Failed to log in")
    }

    setLoading(false)
  }
   
  return (
    <Container>
      <Logo component={Link} to={'/'}>Tranood</Logo>
      <Wrapper>
        <Form>
        <Title>SIGN IN</Title>
        {error && <Alert variant="danger">{error}</Alert>}
          <InputView>
            <MailOutlined style={{color: greyMedium}}/>
            <Input placeholder="Email" ref={emailRef} />
          </InputView>
          <InputView>
            <LockOutlined style={{color: greyMedium}}/>
            <Input
              placeholder="Password"
              type={passwordVisibility}
              ref={passwordRef}
            />
            {passwordVisibility === "password"
              ? <VisibilityOutlined style={{color: greyMedium}} onClick={handlePasswordVisibility}/>
              : <VisibilityOffOutlined style={{color: greyMedium}} onClick={handlePasswordVisibility}/>
            }
          </InputView>
          <Button onClick={handleSubmit} disabled={loading}>LOGIN</Button>
          <StyledLink>Forgot Password</StyledLink>
          <AccountQuestion>
            Don't have an account? <Link to="/register">Register</Link>
          </AccountQuestion>
        </Form>
      </Wrapper>
    </Container>
  )
}

export default Login