import React from 'react'
import Navbar from "../components/Navbar";
import Slider from '../components/Slider';
import Newsletter from '../components/Newsletter';
import Footer from '../components/Footer';
import Announcement from '../components/Announcement';

export const Home = () => {
  return (
    <div>
      <Announcement/>
      <Navbar/>
      <Slider/>
      <Newsletter/>
      <Footer/>
    </div>
  )
}

export default Home
