import { useEffect, useState, Fragment } from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import { publicRequest } from "../requestMethods";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import CardContent from "@mui/material/CardContent";
import IconButton from "@material-ui/core/IconButton";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import "../css/styles.css";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Box from "@mui/material/Box";
import Button from "@material-ui/core/Button";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import { useAuth } from "../contexts/AuthContext";
import axios from "axios";
import { API_URL } from "../constants/paths";

const Container = styled.div``;

const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Title = styled.div`
  padding: 16px;
  font-size: 24px;
  font-weight: 800;
  align-self: center;
`;

export default function Explore() {
  const [refugeesPosts, setRefugeesPosts] = useState([]);
  const [expandedId, setExpandedId] = useState(-1);
  const [allUsers, setAllUsers] = useState([]);
  const { currentUser } = useAuth();

  const handleExpandClick = (i) => {
    setExpandedId(expandedId === i ? -1 : i);
  };

  const getRefugeeName = authorId => {
    for (let i = 0; i < allUsers.length; i++) {
      if(allUsers[i].firebase_uid === authorId) {
        return allUsers[i].name;
      }
    }
    return "Unknown"
  }

  const getAllUsers = async () => {
    try {
      await axios
        .get(`${API_URL}/users`)
        .then((res) => {
          console.log(res.data)
          setAllUsers(res.data);
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  }

  const makeRequest = async (itemId) => {
    try {
      console.log(itemId);
      console.log(currentUser.uid);
      const token = await currentUser.getIdToken();
      const body = {};
      const headers = {};
      headers["headers"] = { Authorization: `Bearer ${token}` };
      await axios
        .put(`${API_URL}/items/${itemId}/request`, body, headers)
        .then((res) => {
          console.log(res.data);
          getRefugeesPosts();
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  };

  const bull = (
    <Box
      component="span"
      sx={{ display: "inline-block", mx: "2px", transform: "scale(0.8)" }}
    >
      •
    </Box>
  );

  const getRefugeesPosts = async () => {
    console.log("Donator id: " + currentUser.uid);
    try {
      await axios
        .get(`${API_URL}/posts/items`)
        .then((res) => {
          console.log(res.data);
          setRefugeesPosts(res.data);
        })
        .catch((err) => {
          console.log("error", err.message);
        });
    } catch {}
  };

  useEffect(() => {
    getRefugeesPosts();
    getAllUsers();
  }, []);

  return (
    <Container>
      <Navbar />
      <Row>
        <Title>View Active Posts</Title>
      </Row>
      <div className="posts">
        {refugeesPosts
          .filter(function (el) {
            let res = false;
            const items = el.items;
            for (let item = 0; item < items.length; item++) {
              if (items[item].active === true) {
                res = true;
                break;
              }
            }
            return res;
          })
          .map((x, i) => {
            return (
              <Card
                key={i}
                className="postCard"
                variant="outlined"
                sx={{ minWidth: 275 }}
              >
                <CardContent>
                  <CardActions disableSpacing>
                    <Typography variant="h5" component="div">
                      Post {i + 1} from {getRefugeeName(x.post.author)}
                    </Typography>

                    <IconButton
                      aria-label="show more"
                      onClick={() => handleExpandClick(i)}
                      aria-expanded={expandedId === i}
                    >
                      <ExpandMoreIcon />
                    </IconButton>
                  </CardActions>

                  {x.items
                    .filter(function (el) {
                      return el.active === true;
                    })
                    .map((y, j) => {
                      return (
                        <Collapse
                          key={j}
                          in={expandedId === i}
                          style={{
                            opacity: !y.requested.includes(currentUser.uid)
                              ? 1.0
                              : 0.5,
                          }}
                          timeout="auto"
                          unmountOnExit
                        >
                          <Card
                            className="itemCard"
                            variant="outlined"
                            sx={{ minWidth: 275 }}
                            style={{ opacity: y.active ? 1.0 : 0.5 }}
                          >
                            <CardContent>
                              <Typography variant="h5" component="div">
                                Item {j + 1}
                              </Typography>
                              {y.name} {bull}
                              {y.tag}
                              {bull}
                              <br />
                              <br />
                              <Typography
                                sx={{ mb: 1.5 }}
                                color="text.secondary"
                              >
                                {y.description}
                              </Typography>
                            </CardContent>
                            <CardActions>
                              {!y.requested.includes(currentUser.uid) ? (
                                <Button
                                  size="small"
                                  variant="contained"
                                  className="inactiveButton"
                                  onClick={() => makeRequest(y._id)}
                                >
                                  Submit
                                </Button>
                              ) : (
                                <Typography
                                  className="alreadyInactive"
                                  sx={{ mb: 1.5 }}
                                  color="text.secondary"
                                >
                                  Submitted
                                </Typography>
                              )}
                            </CardActions>
                          </Card>
                        </Collapse>
                      );
                    })}
                </CardContent>
              </Card>
            );
          })}
      </div>
      <Footer />
    </Container>
  );
}
