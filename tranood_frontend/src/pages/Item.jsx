import React from 'react'
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from '@mui/material/MenuItem';
import { Paper } from "@material-ui/core";
import Button from "@material-ui/core/Button";

export default function Item({name, tag, description, index, handleInputChange}) {

	const tags = [{value: "donation", label: "donation"}, {value: "transport", label: "transport"}, {value: "shelter", label: "shelter"}]

  return (
		<div className='field'>
			<Grid item >
				<TextField
					fullWidth
					required
					label="Name"
					name="name"
					value={name}
					onChange={e => handleInputChange(e, index)}
				/>
			</Grid>
			<Grid item >
				<TextField
					fullWidth
					required
					id="outlined-select-currency"
					select
					label="Tag"
					name="tag"
					value={tag}
					onChange={e => handleInputChange(e, index)}
				>
				{tags.map((option) => (
					<MenuItem key={option.value} value={option.value}>
						{option.label}
					</MenuItem>
				))}
				</TextField>
			</Grid>
			<Grid item >
				<TextField
					fullWidth 
					id="outlined-multiline-flexible"
					label="Description"
					multiline
					maxRows={4}
					name="description"
					value={description}
					onChange={e => handleInputChange(e, index)}
				/>
			</Grid>
		</div>
  )
}
