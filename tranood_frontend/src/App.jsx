import React, { useLayoutEffect } from "react";
import { BrowserRouter as Router, Route, Routes, Navigate, useLocation } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Success from "./pages/Success";
import SupportUkraine from "./pages/SupportUkraine";
import Register from "./pages/Register";
import Profile from "./pages/Profile";
import { useSelector } from "react-redux";
import { AuthProvider } from "./contexts/AuthContext";
import AddPost from "./pages/AddPost";
import ViewPosts from "./pages/ViewPosts";
import Explore from "./pages/Explore";

const ScrollToTop = ({children}) => {
  const location = useLocation();
  useLayoutEffect(() => {
    document.documentElement.scrollTo(0, 0);
  }, [location.pathname]);
  return children
}

const App = () => {
  const user = useSelector((state) => state.user.currentUser)
  return (
    <AuthProvider>
      <Router>
        <ScrollToTop>
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route path="/post/new" element={<AddPost/>}/>
            <Route path="/viewPosts" element={<ViewPosts/>}/>
            <Route path="/explore" element={<Explore/>}/>
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/login" element={user ? <Navigate replace to="/"/> : <Login/>}/>
            <Route path="/register" element={user ? <Navigate replace to="/"/> : <Register/>}/>
            <Route path="/support_ukraine" element={<SupportUkraine/>}/>
            <Route path="/success" element={<Success/>}/>
          </Routes>
        </ScrollToTop>
      </Router>
    </AuthProvider>
  );
};

export default App;
