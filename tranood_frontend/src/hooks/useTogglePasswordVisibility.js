import { useState } from "react";

export const useTogglePasswordVisibility = () => {
  const [passwordVisibility, setPasswordVisibility] = useState("password");

  const handlePasswordVisibility = () => {
    if (passwordVisibility === "password") {
      setPasswordVisibility("text");
    } else if (passwordVisibility === "text") {
      setPasswordVisibility("password");
    }
  };

  return {
    passwordVisibility,
    handlePasswordVisibility
  };
};
