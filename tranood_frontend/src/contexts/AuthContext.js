import React, { useContext, useState, useEffect } from "react"
import { auth } from "../firebase"
import axios from "axios";
import { API_URL } from "../constants/paths";

const AuthContext = React.createContext()

export function useAuth() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [currentUser, setCurrentUser] = useState()
  const [profile, setProfile] = useState()
  const [loading, setLoading] = useState(true)

  function signup(email, password) {
    return auth.createUserWithEmailAndPassword(email, password)
  }

  function login(email, password) {
    return auth.signInWithEmailAndPassword(email, password)
  }

  function logout() {
    return auth.signOut()
  }

  function resetPassword(email) {
    return auth.sendPasswordResetEmail(email)
  }

  function updateEmail(email) {
    return currentUser.updateEmail(email)
  }

  function updatePassword(password) {
    return currentUser.updatePassword(password)
  }

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(async function(user) {
      setCurrentUser(user)
      if (user) {
        var token = await user.getIdToken();
    //    console.log(token)
        await axios.get(`${API_URL}/auth/profile`, {
          headers: {
            'Authorization' : `Bearer ${token}`
          }
        }).then(res => {
          setProfile(res.data)
        }).catch(err => {
          console.log("error", err.message)
        })
      } else {
        setProfile(null);
      }
      setLoading(false)
    })

    return unsubscribe
  }, [])

  const value = {
    currentUser,
    profile,
    login,
    signup,
    logout,
    resetPassword,
    updateEmail,
    updatePassword
  }

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
